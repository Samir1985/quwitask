package com.quwi.core.apiservice;

import com.quwi.core.models.LoginDTO;
import com.quwi.core.models.ProjectUpdateDTO;
import com.quwi.core.models.ProjectUpdateResponseDTO;
import com.quwi.core.models.ProjectsListDTO;
import com.quwi.core.models.TokenDTO;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService {
	@POST("auth/login")
	Call<TokenDTO> login(@Body LoginDTO loginDTO);

	@GET("projects-manage/index")
	Call<ProjectsListDTO> projects();

	@POST("projects-manage/update")
	Call<ProjectUpdateResponseDTO> update(@Query("id") Integer id,
										  @Body ProjectUpdateDTO updateInfo);
}
