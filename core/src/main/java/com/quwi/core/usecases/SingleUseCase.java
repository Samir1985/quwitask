package com.quwi.core.usecases;

import io.reactivex.Single;

interface SingleUseCase<R> {
	Single<R> execute();
}
