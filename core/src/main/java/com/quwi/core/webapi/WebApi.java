package com.quwi.core.webapi;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import javax.inject.Singleton;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Singleton
public class WebApi {
	private final OkHttpClient okHttpClient =
		new OkHttpClient.Builder().addInterceptor(new HttpLoggingInterceptor()).build();

	private Retrofit retrofit = new Retrofit.Builder()
		.baseUrl("https://api.quwi.com/v2/")
		.addConverterFactory(JacksonConverterFactory.create())
		.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
		.client(okHttpClient)
		.build();

	public void setToken(String token) {

		retrofit = new Retrofit.Builder()
			.baseUrl("https://api.quwi.com/v2/")
			.addConverterFactory(JacksonConverterFactory.create())
			.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
			.client(setTokenHeader(token))
			.build();
	}

	private OkHttpClient setTokenHeader(final String token) {
		OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();

		return httpClientBuilder.addInterceptor(new Interceptor() {
			@NotNull
			@Override
			public Response intercept(@NotNull Chain chain) throws IOException {
				Request origin = chain.request();

				Request request = origin.newBuilder().header("Authorization", "Bearer " + token).build();

				return chain.proceed(request);
			}
		}).build();
	}

	public Retrofit getRetrofit() {
		return retrofit;
	}

	public void setRetrofit(Retrofit retrofit) {
		this.retrofit = retrofit;
	}
}
