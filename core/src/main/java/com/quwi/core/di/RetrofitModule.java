package com.quwi.core.di;

import com.quwi.core.webapi.WebApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RetrofitModule {
	@Provides
	@Singleton
	WebApi webApi() {
		return new WebApi();
	}
}
