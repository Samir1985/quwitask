package com.quwi.core.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FirstError {
	@JsonProperty("first_errors")
	LoginDTO firstError;

	public LoginDTO getFirstError() {
		return firstError;
	}
}
