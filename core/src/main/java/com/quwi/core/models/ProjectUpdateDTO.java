package com.quwi.core.models;

public class ProjectUpdateDTO {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
