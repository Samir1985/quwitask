package com.quwi.core.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectDTO {
	public Integer id;
	public String name;
	public String uid;
	public String logo_url;
	public Integer position;
	public Integer is_active;
	public Boolean is_owner_watcher;
	public String dta_user_since;
	public List<UserDTO> users;
}
