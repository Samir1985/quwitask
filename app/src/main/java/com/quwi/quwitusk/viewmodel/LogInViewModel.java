package com.quwi.quwitusk.viewmodel;

import android.widget.Button;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.quwi.core.apiservice.ApiService;
import com.quwi.core.models.FirstError;
import com.quwi.core.models.LoginDTO;
import com.quwi.core.models.TokenDTO;
import com.quwi.core.webapi.WebApi;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogInViewModel extends ViewModel {
	private final MutableLiveData<TokenDTO> mutableLiveData = new MutableLiveData<>();
	private final MutableLiveData<FirstError> errorData = new MutableLiveData<>();

	public void init(LoginDTO loginDTO, Button loginBtn) {
		mutableLiveData.setValue(null);
		errorData.setValue(null);

		ApiService apiService = new WebApi().getRetrofit().create(ApiService.class);
		apiService.login(loginDTO).enqueue(new Callback<TokenDTO>() {
			@Override
			public void onResponse(@NotNull Call<TokenDTO> call,
								   @NotNull Response<TokenDTO> response) {
				if (mutableLiveData.getValue() == null) {
					mutableLiveData.setValue(response.body());
				}

				if (response.errorBody() != null) {
					loginBtn.setEnabled(true);

					try {
						FirstError error =
							new ObjectMapper()
								.readValue(response.errorBody().string(), FirstError.class);
						errorData.setValue(error);

					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			@Override
			public void onFailure(@NotNull Call<TokenDTO> call, @NotNull Throwable t) {
				loginBtn.setEnabled(true);
				Throwable throwable = t;
			}
		});
	}

	public LiveData<TokenDTO> getTokenData() {
		return mutableLiveData;
	}

	public LiveData<FirstError> getErrorData() {
		return errorData;
	}
}
