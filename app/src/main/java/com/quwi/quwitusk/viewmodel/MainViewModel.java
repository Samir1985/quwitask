package com.quwi.quwitusk.viewmodel;

import androidx.lifecycle.ViewModel;

import io.reactivex.disposables.CompositeDisposable;

public class MainViewModel extends ViewModel {
	private CompositeDisposable compositeDisposable() {
		return new CompositeDisposable();
	}

	public void getData(){}
}
