package com.quwi.quwitusk.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.quwi.core.apiservice.ApiService;
import com.quwi.core.models.ProjectDTO;
import com.quwi.core.models.ProjectUpdateDTO;
import com.quwi.core.models.ProjectUpdateResponseDTO;
import com.quwi.core.webapi.WebApi;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProjectDetailViewModel extends ViewModel {
	private final MutableLiveData<ProjectDTO> project = new MutableLiveData<>();
	private final MutableLiveData<ProjectDTO> mutableLiveData = new MutableLiveData<>();

	public LiveData<ProjectDTO> getProjectsData() {
		return project;
	}

	public void setProjectValue(ProjectDTO newValue) {
		project.setValue(newValue);
	}


	public void update(String token, Integer id, ProjectUpdateDTO updateInfo) {
		WebApi api = new WebApi();
		api.setToken(token);

		ApiService apiService = api.getRetrofit().create(ApiService.class);
		apiService.update(id, updateInfo).enqueue(new Callback<ProjectUpdateResponseDTO>() {
			@Override
			public void onResponse(@NotNull Call<ProjectUpdateResponseDTO> call,
								   @NotNull Response<ProjectUpdateResponseDTO> response) {
				mutableLiveData.setValue(response.body().getProject());
			}

			@Override
			public void onFailure(@NotNull Call<ProjectUpdateResponseDTO> call, @NotNull Throwable t) {
				Throwable throwable = t;
			}
		});
	}

	public LiveData<ProjectDTO> getUpdatedData() {
		return mutableLiveData;
	}
}
