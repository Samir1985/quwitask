package com.quwi.quwitusk.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.quwi.core.apiservice.ApiService;
import com.quwi.core.models.ProjectsListDTO;
import com.quwi.core.webapi.WebApi;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProjectsViewModel extends ViewModel {
	private final MutableLiveData<ProjectsListDTO> mutableLiveData = new MutableLiveData<>();

	public void init(String token) {
		WebApi api = new WebApi();
		api.setToken(token);

		ApiService apiService = api.getRetrofit().create(ApiService.class);
		apiService.projects().enqueue(new Callback<ProjectsListDTO>() {
			@Override
			public void onResponse(@NotNull Call<ProjectsListDTO> call,
								   @NotNull Response<ProjectsListDTO> response) {
				mutableLiveData.setValue(response.body());
			}

			@Override
			public void onFailure(@NotNull Call<ProjectsListDTO> call, @NotNull Throwable t) {
				Throwable throwable = t;
			}
		});
	}

	public LiveData<ProjectsListDTO> getProjectsData() {
		return mutableLiveData;
	}
}
