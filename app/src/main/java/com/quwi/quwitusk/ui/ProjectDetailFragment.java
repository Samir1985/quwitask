package com.quwi.quwitusk.ui;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.makeramen.roundedimageview.RoundedImageView;
import com.quwi.core.models.ProjectDTO;
import com.quwi.core.models.ProjectUpdateDTO;
import com.quwi.quwitusk.R;
import com.quwi.quwitusk.viewmodel.ProjectDetailViewModel;
import com.squareup.picasso.Picasso;

public class ProjectDetailFragment extends Fragment {
	public ProjectDetailFragment() {
		super(R.layout.fragment_project_detail);
	}

	private ProjectDetailViewModel viewModel;
	private ProjectDTO project;

	private TextView projectTitle;
	private SwitchCompat isActiveSC;
	private SwitchCompat isOwnerCB;
	private ImageButton editIB;
	private RoundedImageView projectPicRIV;
	private TextView backTV;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
							 @Nullable Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		viewModel = new ViewModelProvider(requireActivity()).get(ProjectDetailViewModel.class);
		project = viewModel.getProjectsData().getValue();

		projectTitle = view.findViewById(R.id.projectName);
		isActiveSC = view.findViewById(R.id.isActiveSC);
		isOwnerCB = view.findViewById(R.id.isOwnerSC);
		editIB = view.findViewById(R.id.editIB);
		projectPicRIV = view.findViewById(R.id.projectDetailPicRIV);
		backTV = view.findViewById(R.id.projectDetailBackTV);

		setInfo();
	}

	private void setInfo() {
		projectTitle.setText(project.name);
		if (project.logo_url == null) {
			Picasso.get()
				.load(String.format("%s%s%s", "https://icotar.com/initials/",
					project.name, ".png?s=100"))
				.into(projectPicRIV);
		} else {
			Picasso.get().load(project.logo_url).into(projectPicRIV);
		}
		isActiveSC.setChecked(project.is_active != 0);
		isOwnerCB.setChecked(project.is_owner_watcher);

		editIB.setOnClickListener(view -> createEditDialog());
		backTV.setOnClickListener(view1 -> {
			Fragment fragmentContainer =
				requireActivity().getSupportFragmentManager()
					.findFragmentById(R.id.fragmentContainer);
			if (fragmentContainer != null) {
				((NavHostFragment) fragmentContainer).getNavController().popBackStack();
			}
		});
	}

	private void createEditDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		LayoutInflater inflater = LayoutInflater.from(getContext());
		View editDialogView = inflater.inflate(R.layout.project_detail_dialog_edit, null);
		builder.setView(editDialogView);

		builder.setTitle("Change projects name");

		builder.setPositiveButton("Save", (dialog, which) -> {
			EditText editText = editDialogView.findViewById(R.id.projectNameET);
			String edited = editText.getText().toString().trim();

			if (!edited.isEmpty()) {
				ProjectUpdateDTO updateInfo = new ProjectUpdateDTO();
				updateInfo.setName(edited);

				SharedPreferences shared = requireContext().getSharedPreferences("PREFS", 0);
				String token = shared.getString("token", null);

				viewModel.update(token, project.id, updateInfo);
				viewModel.getUpdatedData().observe(requireActivity(), updateResponse -> {
					if (updateResponse != null) {
						project.name = updateResponse.name;
						projectTitle.setText(project.name);
						if (project.logo_url == null) {
							Picasso.get()
								.load(String.format("%s%s%s", "https://icotar.com/initials/",
									project.name, ".png?s=100"))
								.into(projectPicRIV);
						} else {
							Picasso.get().load(project.logo_url).into(projectPicRIV);
						}
					}
				});
			}
		});
		builder.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());

		builder.create().show();

		EditText editText = editDialogView.findViewById(R.id.projectNameET);
		editText.setText(project.name);
	}
}
