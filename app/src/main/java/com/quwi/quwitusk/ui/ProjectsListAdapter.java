package com.quwi.quwitusk.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.quwi.core.models.ProjectDTO;
import com.quwi.quwitusk.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

class ProjectsListAdapter extends RecyclerView.Adapter<ProjectsListAdapter.ViewHolder> {
	private ProjectsListFragment.ProjectClick onElementClick;

	public ProjectsListAdapter() {
		super();
	}

	public ProjectsListAdapter(ProjectsListFragment.ProjectClick onElementClick) {
		super();
		this.onElementClick = onElementClick;
	}

	private List<ProjectDTO> list = new ArrayList<>();

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return new ViewHolder(
			LayoutInflater.from(parent.getContext())
				.inflate(R.layout.list_element_projects, parent, false)
		);
	}

	@Override
	public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
		ProjectDTO element = list.get(position);

		if (element.logo_url == null) {
			Picasso.get()
				.load(String.format("%s%s%s", "https://icotar.com/initials/",
					element.name, ".png?s=100"))
				.into(holder.projectPicRIV);
		} else {
			Picasso.get().load(element.logo_url).into(holder.projectPicRIV);
		}
		holder.projectName.setText(element.name);
		holder.elementCL.setOnClickListener(view -> onElementClick.onElementClick(element));
	}

	@Override
	public int getItemCount() {
		return list.size();
	}

	public void setList(List<ProjectDTO> list) {
		this.list = list;
		notifyDataSetChanged();
	}

	static class ViewHolder extends RecyclerView.ViewHolder {
		ViewHolder(View itemView) {
			super(itemView);
		}

		public ConstraintLayout elementCL = itemView.findViewById(R.id.elementCL);
		public TextView projectName = itemView.findViewById(R.id.projectName);
		public RoundedImageView projectPicRIV = itemView.findViewById(R.id.projectPicRIV);
	}
}
