package com.quwi.quwitusk.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;
import com.quwi.core.models.LoginDTO;
import com.quwi.quwitusk.R;
import com.quwi.quwitusk.viewmodel.LogInViewModel;

public class LogInFragment extends Fragment {
	public LogInFragment() {
		super(R.layout.fragment_log_in);
	}

	private LogInViewModel viewModel;

	private Button logInBtn;
	private EditText emailET;
	private EditText passwordET;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
							 @Nullable Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		viewModel = new ViewModelProvider(this).get(LogInViewModel.class);

		emailET = view.findViewById(R.id.emailET);
		passwordET = view.findViewById(R.id.passwordET);
		logInBtn = view.findViewById(R.id.logInBtn);

		logInBtn.setOnClickListener(view1 -> {
			view1.setEnabled(false);

			LoginDTO loginDTO = new LoginDTO();
			loginDTO.setEmail(emailET.getText().toString());
			loginDTO.setPassword(passwordET.getText().toString());

			viewModel.init(loginDTO, logInBtn);
			viewModel.getTokenData().observe(requireActivity(), tokenResponse -> {
				if (tokenResponse != null) {
					String token = tokenResponse.getToken();
					SharedPreferences shared = requireContext().getSharedPreferences("PREFS", 0);
					shared.edit().putString("token", token).apply();

					Fragment fragmentContainer =
						requireActivity().getSupportFragmentManager()
							.findFragmentById(R.id.fragmentContainer);
					if (fragmentContainer != null) {
						NavController navController =
							((NavHostFragment) fragmentContainer).getNavController();
						if (navController.getCurrentDestination().getId() == R.id.logInFragment) {
							navController.navigate(R.id.action_logInFragment_to_projectsListFragment);
						}
					}
				}
			});
			viewModel.getErrorData().observe(requireActivity(), firstError -> {
				if (firstError != null) {

					String emailError = "";
					if (firstError.getFirstError().getEmail() != null) {
						emailError = firstError.getFirstError().getEmail();
					}
					String passwordError = "";
					if (firstError.getFirstError().getPassword() != null) {
						passwordError = firstError.getFirstError().getPassword();
					}

					String message =
						String.format(
							"%s %s",
							emailError,
							passwordError
						);

					Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show();
				}
			});
		});
	}

	@Override
	public void onPause() {
		super.onPause();

		viewModel.getTokenData().removeObservers(requireActivity());
		viewModel.getErrorData().removeObservers(requireActivity());
	}
}
