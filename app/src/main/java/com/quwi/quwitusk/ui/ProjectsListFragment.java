package com.quwi.quwitusk.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import com.quwi.core.models.ProjectDTO;
import com.quwi.quwitusk.R;
import com.quwi.quwitusk.viewmodel.ProjectDetailViewModel;
import com.quwi.quwitusk.viewmodel.ProjectsViewModel;

public class ProjectsListFragment extends Fragment {
	public ProjectsListFragment() {
		super(R.layout.fragment_projects);
	}

	private ProjectsViewModel viewModel;
	private ProjectDetailViewModel projectDetailViewModel;

	private TextView backTV;
	private RecyclerView projectsRV;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
							 @Nullable Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		SharedPreferences shared = requireContext().getSharedPreferences("PREFS", 0);
		String token = shared.getString("token", null);

		viewModel = new ViewModelProvider(requireActivity()).get(ProjectsViewModel.class);
		projectDetailViewModel = new ViewModelProvider(requireActivity()).get(ProjectDetailViewModel.class);

		setViews();

		viewModel.init(token);
		viewModel.getProjectsData().observe(requireActivity(), tokenResponse -> {
			if (tokenResponse != null) {
				((ProjectsListAdapter) projectsRV.getAdapter()).setList(tokenResponse.projects);
			}
		});

	}

	private void setViews() {
		backTV = requireActivity().findViewById(R.id.backTV);
		projectsRV = requireActivity().findViewById(R.id.projectsRV);

		backTV.setOnClickListener(view1 -> {
			Fragment fragmentContainer =
				requireActivity().getSupportFragmentManager()
					.findFragmentById(R.id.fragmentContainer);
			if (fragmentContainer != null) {
				((NavHostFragment) fragmentContainer).getNavController().popBackStack();
			}
		});

		projectsRV.addItemDecoration(
			new DividerItemDecoration(requireContext(), RecyclerView.VERTICAL)
		);

		ProjectsListAdapter adapter = new ProjectsListAdapter(project -> {
			projectDetailViewModel.setProjectValue(project);

			Fragment fragmentContainer =
				requireActivity().getSupportFragmentManager()
					.findFragmentById(R.id.fragmentContainer);
			if (fragmentContainer != null) {
				((NavHostFragment) fragmentContainer).getNavController()
					.navigate(R.id.action_projectsListFragment_to_projectDetailFragment);
			}
		});
		projectsRV.setAdapter(adapter);
	}

	interface ProjectClick {
		void onElementClick(ProjectDTO project);
	}
}
